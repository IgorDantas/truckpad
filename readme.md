# Truckpad

> Truckpad Project Challenge

## Building

``` bash
source venv/bin/activate
python manage.py migrate

# Start app
python manage.py runserver

```

## Test

* for test use at browser 127.0.0.1:8000

## Precisamos criar uma api para cadastrar os motoristas que chegam nesse terminal e saber mais informações sobre eles. Precisamos saber nome, idade, sexo, se possui veiculo, tipo da ​CNH​, se está carregado, tipo do veiculo que está dirigindo.
## Precisamos saber a origem e destino de cada caminhoneiro. Será necessário pegar a latitude e longitude de cada origem e destino.
## Será necessário atualizar os registros dos caminhoneiros.
* POST -> 127.0.0.1:8000/driver
* GET -> 127.0.0.1:8000/driver/
* GET -> 127.0.0.1:8000/driver/:id
* PUT -> 127.0.0.1:8000/driver/:id

## Precisamos de um método para consultar todos os motoristas que não tem carga para voltar ao seu destino de origem.
* GET -> 127.0.0.1:8000/driver/?full_vehicle=False

## Precisamos saber quantos caminhões passam carregados pelo terminal durante o dia, semana e mês.
# Lista
* GET dia (format: YYYY-MM-DD) -> 127.0.0.1:8000/driver/?created_at=:date
* GET semana (format: YYYY-MM-DD) -> 127.0.0.1:8000/driver/?start_date=:startdate&end_date=:enddate
* GET mês (format: YYYY-MM-DD) -> 127.0.0.1:8000/driver/?start_date=:startdate&end_date=:enddate
# Quantidade
* GET dia, semana (format: YYYY-MM-DD) -> 127.0.0.1:8000/driver/?start_date=:startdate&end_date=:enddate&count=True
* GET mês (format: YYYY-MM-DD) -> 127.0.0.1:8000/driver/?start_date=:startdate&end_date=:enddate&count=True

## Precisamos saber quantos caminhoneiros tem veiculo próprio.
* GET -> 127.0.0.1:8000/driver/?group_own_vehicle=True

## Mostrar uma lista de origem e destino agrupado por cada um dos tipos.
* GET -> 127.0.0.1:8000/driver/?group_type_vehicle=True

## Criar testes unitários para validar o funcionamento dos serviços criados. Utilize o ​framework​ de testes de sua preferência.
* 127.0.0.1:8000


For more detail contact me igordantas91@icloud.com.