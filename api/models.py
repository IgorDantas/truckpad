from django.db import models
from django.db.models.query import QuerySet
from django_group_by import GroupByMixin

class DriverQuerySet(QuerySet, GroupByMixin):
    pass

class Driver(models.Model):
    GENDER = (
        ('Masculino', 'Masculino'),
        ('Feminino', 'Feminino'),
    )
    TYPEVEHICLE = (
        ('1','Caminhão 3/4'),
        ('2','Caminhão Toco'),
        ('3','Caminhão ​Truck'),
        ('4','Carreta Simples'),
        ('5','Carreta Eixo Extendido'),
    )
    objects = DriverQuerySet.as_manager()
    name = models.CharField(max_length=500)
    age = models.PositiveSmallIntegerField()
    gender = models.CharField(max_length=100, choices=GENDER)
    cnh = models.CharField(max_length=500)
    own_vehicle = models.BooleanField()
    type_vehicle = models.CharField(max_length=500, choices=TYPEVEHICLE)
    full_vehicle = models.BooleanField()
    origin = models.CharField(max_length=500)
    from_lat = models.DecimalField(max_digits=8, decimal_places=3)
    from_long = models.DecimalField(max_digits=8, decimal_places=3)
    destiny = models.CharField(max_length=500)
    to_lat = models.DecimalField(max_digits=8, decimal_places=3)
    to_long = models.DecimalField(max_digits=8, decimal_places=3)
    created_at = models.DateField(auto_now_add=True)
    updated_at = models.DateField(auto_now=True)
    
    def __str__(self):
        return self.name

    @property
    def own_vehicle_count(self):
        return self.own_vehicle_count.count

    @property
    def full_vehicle_count(self):
        return self.full_vehicle_count.count
