# from django.shortcuts import render 
from rest_framework import viewsets
from .models import Driver
from .serializers import DriverSerializer, DriverGroupSerializer,DriverCountSerializer, DriverGroupTypeVehicleSerializer
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.filters import OrderingFilter, SearchFilter
from django.db.models import Count, Sum
import datetime

class DriverView(viewsets.ModelViewSet):
    serializer_class = DriverSerializer
    queryset = Driver.objects.all()

    def get_queryset(self):
        result = self.request.query_params.get('group_own_vehicle', None)
        if result is not None:
            # group_own_vehicle
            queryset = Driver.objects.values('own_vehicle').annotate(own_vehicle_count=Count('own_vehicle')).distinct()
        else:
            start_date = self.request.query_params.get('start_date', None)
            end_date = self.request.query_params.get('end_date', None)
            count = self.request.query_params.get('count', None)
            if (start_date is not None and end_date is not None and count is not None):
                # created_at__range COUNT
                queryset = Driver.objects.filter(created_at__range=(start_date, end_date)).values('full_vehicle').annotate(full_vehicle_vcount=Count('full_vehicle')).distinct()
            else:
                if (start_date is not None and end_date is not None):
                    # created_at__range
                    queryset = Driver.objects.filter(created_at__range=(start_date, end_date))
                else:
                    group_type_vehicle = self.request.query_params.get('group_type_vehicle', None)
                    if group_type_vehicle is not None:
                        # group type vehicle
                        queryset = Driver.objects.values('type_vehicle', 'origin', 'destiny').distinct()
                    else:
                        queryset = Driver.objects.all()
        return queryset

    def get_serializer_class(self):
        result = self.request.query_params.get('group_own_vehicle', None)
        if result is not None:
            serializer_class = DriverGroupSerializer
        else:
            start_date = self.request.query_params.get('start_date', None)
            end_date = self.request.query_params.get('end_date', None)
            count = self.request.query_params.get('count', None)
            if (start_date is not None and end_date is not None and count is not None):
                serializer_class = DriverCountSerializer
            else:
                if (start_date is not None and end_date is not None):
                    serializer_class = DriverSerializer
                else:
                    group_type_vehicle = self.request.query_params.get('group_type_vehicle', None)
                    if group_type_vehicle is not None:
                        serializer_class = DriverGroupTypeVehicleSerializer
                    else:
                        serializer_class = DriverSerializer
        return serializer_class
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)
    filter_fields = ('id', 'name', 'age', 'gender', 'own_vehicle', 'cnh', 'type_vehicle', 'full_vehicle', 'origin', 'from_lat', 'from_long', 'destiny', 'to_lat', 'to_long', 'created_at', 'updated_at')
    ordering_fields = ('id', 'name', 'age', 'gender', 'own_vehicle', 'cnh', 'type_vehicle', 'full_vehicle', 'origin', 'from_lat', 'from_long', 'destiny', 'to_lat', 'to_long', 'created_at', 'updated_at')
    search_fields = ('id', 'name', 'age', 'gender', 'own_vehicle', 'cnh', 'type_vehicle', 'full_vehicle', 'origin', 'from_lat', 'from_long', 'destiny', 'to_lat', 'to_long', 'created_at', 'updated_at')