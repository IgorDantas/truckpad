from rest_framework import serializers
from .models import Driver

class DriverSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = ('id', 'name', 'url', 'age', 'gender', 'own_vehicle', 'cnh', 'type_vehicle', 'full_vehicle', 'origin', 'from_lat', 'from_long', 'destiny', 'to_lat', 'to_long', 'created_at', 'updated_at')

class DriverGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = ('own_vehicle','own_vehicle_count')

    def get_own_vehicle_count(self, obj):
        return obj.own_vehicle_set.count()

class DriverCountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = ('full_vehicle','full_vehicle_count')

    def get_full_vehicle_count(self, obj):
        return obj.full_vehicle_set.count()

class DriverGroupTypeVehicleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Driver
        fields = ('type_vehicle', 'origin', 'destiny')